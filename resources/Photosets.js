module.exports = {
    sets: function () {
        return [
            {
                id: 1,
                photoset_order: 6452,
                photoset_id: "72157667940870148",
                title: "Inauguración Hotel HAMPTON by Hilton",
                is_public: 1
            },
            {
                id: 2,
                photoset_order: 6451,
                photoset_id: "72157695073329384",
                title: "Cancino Primero, Santo Dominigo Este. Will Herrera",
                is_public: 1
            },
            {
                id: 3,
                photoset_order: 6450,
                photoset_id: "72157692646232642",
                title: "Entrevista Flavio Darío Espinal, Para Enfoque Matinal, 37",
                is_public: 1
            },
            {
                id: 4,
                photoset_order: 6449,
                photoset_id: "72157689993289920",
                title: "Gobierno entrega pozos para servir agua potable en varios sectores de Pedro Brand",
                is_public: 1
            },
            {
                id: 5,
                photoset_order: 6448,
                photoset_id: "72157667917511958",
                title: "Países del SICA adoptarán modelo de compras públicas dominicano",
                is_public: 1
            },
            {
                id: 6,
                photoset_order: 6447,
                photoset_id: "72157693978830581",
                title: "Transformación sector salud se consolida en Sánchez Ramírez; Danilo entrega nuevo hospital a Fantino",
                is_public: 1
            },
            {
                id: 7,
                photoset_order: 6446,
                photoset_id: "72157689983507550",
                title: "Danilo Medina da seguimiento a construcción escuelas y estancias infantiles",
                is_public: 1
            },
            {
                id: 8,
                photoset_order: 6445,
                photoset_id: "72157667902392818",
                title: "Ramón Tejada Holguín: 80% de inmigrantes que residen en el país se ubican en edad productiva",
                is_public: 1
            },
            {
                id: 9,
                photoset_order: 6444,
                photoset_id: "72157695752578115",
                title: "Flavio Darío Espinal: Encuesta Nacional Inmigrantes arroja luz para concebir políticas de respuesta",
                is_public: 1
            },
            {
                id: 10,
                photoset_order: 6443,
                photoset_id: "72157689976148700",
                title: "Tejada Holguín: de 2012 a 2017 migración extranjera ha sido estable y no significativa",
                is_public: 1
            },
            {
                id: 11,
                photoset_order: 6442,
                photoset_id: "72157695732506425",
                title: "Gobierno implementa medidas preventivas para garantizar producción de vegetales en Rancho Arriba",
                is_public: 1
            },
            {
                id: 12,
                photoset_order: 6441,
                photoset_id: "72157693942893571",
                title: "Josué Fiallo valora fiabilidad de Segunda Encuesta Nacional de Inmigrantes (ENI-2017)",
                is_public: 1
            },
            {
                id: 13,
                photoset_order: 6440,
                photoset_id: "72157695727582425",
                title: "Destacados músicos elogian Ángeles de la Cultura, espacio que desarrolla talento artístico niñez RD",
                is_public: 1
            },
            {
                id: 14,
                photoset_order: 6439,
                photoset_id: "72157695720988285",
                title: "MEPyD y ONE presentan informe preliminar de la Segunda Encuesta Nacional de Inmigrantes (ENI-2017)",
                is_public: 1
            },
            {
                id: 15,
                photoset_order: 6438,
                photoset_id: "72157693901471081",
                title: "BID y Consejo Nacional Competitividad con agenda común en innovación y asociatividad  ",
                is_public: 1
            },
            {
                id: 16,
                photoset_order: 6437,
                photoset_id: "72157695676023435",
                title: "VS202: Danilo ofrece apoyo construcción granja crianza cerdos en Cienfuegos",
                is_public: 1
            },
            {
                id: 17,
                photoset_order: 6436,
                photoset_id: "72157694932758014",
                title: "Danilo regresa de Perú tras agotar amplia agenda de trabajo en VIII Cumbre Las Américas",
                is_public: 1
            },
            {
                id: 18,
                photoset_order: 6435,
                photoset_id: "72157694914053874",
                title: "Presidente Danilo Medina reafirma decidido compromiso con lucha contra corrupción",
                is_public: 1
            },
            {
                id: 19,
                photoset_order: 6434,
                photoset_id: "72157667772182648",
                title: "Danilo Medina y Cándida Montilla de Medina asisten al acto inaugural VIII Cumbre de las Américas",
                is_public: 1
            },
            {
                id: 20,
                photoset_order: 6433,
                photoset_id: "72157693829958731",
                title: "Ejecutivos de PepsiCo. expresan a Danilo Medina interés ampliar operaciones en RD",
                is_public: 1
            },
            {
                id: 21,
                photoset_order: 6432,
                photoset_id: "72157665732728507",
                title: "Primera dama Cándida Montilla de Medina visita a homóloga peruana, Maribel Díaz de Vizcarra",
                is_public: 1
            },
            {
                id: 22,
                photoset_order: 6431,
                photoset_id: "72157695616287095",
                title: "En Perú, Rodríguez Marchena conversa con periodistas dominicanos sobre discurso de Danilo en cumbre",
                is_public: 1
            },
            {
                id: 23,
                photoset_order: 6430,
                photoset_id: "72157667765169708",
                title: "En almuerzo con primer ministro Canadá, Justin Trudeau, Danilo aborda resiliencia climática",
                is_public: 1
            },
            {
                id: 24,
                photoset_order: 6429,
                photoset_id: "72157667762959008",
                title: "Danilo Medina se reúne con Hugo Martínez, canciller de El Salvador",
                is_public: 1
            },
            {
                id: 25,
                photoset_order: 6428,
                photoset_id: "72157689835486010",
                title: "Danilo da seguimiento a préstamos regeneración playas dominicanas y expansión redes eléctricas",
                is_public: 1
            },
            {
                id: 26,
                photoset_order: 6427,
                photoset_id: "72157667758454488",
                title: "En marco de Cumbre Empresarial, Danilo Medina se reúne con ejecutivos de Citibank",
                is_public: 1
            },
            {
                id: 27,
                photoset_order: 6426,
                photoset_id: "72157689812445750",
                title: "Cándida Montilla se reúne con homóloga ecuatoriana; hablan de inclusión personas con discapacidad",
                is_public: 1
            },
            {
                id: 28,
                photoset_order: 6425,
                photoset_id: "72157693792998171",
                title: "Vicepresidenta de Honduras define al CAID como pedacito de cielo que promueve esperanza e inclusión",
                is_public: 1
            },
            {
                id: 29,
                photoset_order: 6424,
                photoset_id: "72157694844458204",
                title: "En Perú, Danilo aboga por justicia, equidad y transparencia en comercio e inversión global",
                is_public: 1
            },
            {
                id: 30,
                photoset_order: 6423,
                photoset_id: "72157694840945274",
                title: "MESCYT y República Digital firman convenio con universidades; impartirán diplomados en tecnología",
                is_public: 1
            },
            {
                id: 31,
                photoset_order: 6422,
                photoset_id: "72157694840275714",
                title: "NP MEM busca mayor integración con Comisión Nacional de Energía",
                is_public: 1
            },
            {
                id: 32,
                photoset_order: 6421,
                photoset_id: "72157667705762558",
                title: "Danilo llega a Perú. Agotará intensa agenda en III Cumbre Empresarial y VIII Cumbre de las Américas",
                is_public: 1
            },
            {
                id: 33,
                photoset_order: 6420,
                photoset_id: "72157692409607152",
                title: "Danilo Medina sale hacia Lima, Perú, rumbo a III Cumbre Empresarial y VIII Cumbre de las Américas",
                is_public: 1
            },
            {
                id: 34,
                photoset_order: 6419,
                photoset_id: "72157693750526571",
                title: "Danilo Medina recibe a enviado comercial del Reino Unido. Destacan fuertes vínculos comerciales",
                is_public: 1
            },
            {
                id: 35,
                photoset_order: 6418,
                photoset_id: "72157693750482211",
                title: "Productores de mangos de Baní reciben camión prometido por Danilo Medina en Visita Sorpresa 201",
                is_public: 1
            },
            {
                id: 36,
                photoset_order: 6417,
                photoset_id: "72157693747186241",
                title: "En Baní, 159 productores reciben proyecto de irrigación fruto de Visita Sorpresa; EGEHID entrega",
                is_public: 1
            },
            {
                id: 37,
                photoset_order: 6416,
                photoset_id: "72157689760661520",
                title: "Danilo recibe inversionistas primera empresa esterilización dispositivos médicos se instalará en RD",
                is_public: 1
            },
            {
                id: 38,
                photoset_order: 6415,
                photoset_id: "72157665634037197",
                title: "Prueba de calidad y tamaño",
                is_public: 1
            },
            {
                id: 39,
                photoset_order: 6414,
                photoset_id: "72157693720204821",
                title: "Despacho Primera Dama presenta conferencia calidad de vida personas con habilidades diferentes",
                is_public: 1
            },
            {
                id: 40,
                photoset_order: 6413,
                photoset_id: "72157667659206248",
                title: "Ministros de agricultura SICA deliberan temas en segunda reunión ordinaria del Consejo de Ministros",
                is_public: 1
            },
            {
                id: 41,
                photoset_order: 6412,
                photoset_id: "72157665620472107",
                title: "Cumpliendo promesa Visita Sorpresa 181, Gobierno inicia trabajos de titulación en Palmar de Ocoa",
                is_public: 1
            },
            {
                id: 42,
                photoset_order: 6411,
                photoset_id: "72157689731736200",
                title: "Presidente hace realidad el sueño de habitantes de Duvergé. Les entrega un moderno hospital",
                is_public: 1
            },
            {
                id: 43,
                photoset_order: 6410,
                photoset_id: "72157665611868307",
                title: "Conadis, Teleférico.",
                is_public: 1
            },
            {
                id: 44,
                photoset_order: 6409,
                photoset_id: "72157693703889771",
                title: "Danilo asiste a inauguración Kingtom Aluminio, primera empresa República Popular China en RD",
                is_public: 1
            },
            {
                id: 45,
                photoset_order: 6408,
                photoset_id: "72157694756406674",
                title: "No había de nada en el hospital municipal José Pérez, Duvergé",
                is_public: 1
            },
            {
                id: 46,
                photoset_order: 6407,
                photoset_id: "72157692340427652",
                title: "Guajimía será saneada en segunda fase, mediante acuerdo con Gobierno Canadá",
                is_public: 1
            },
            {
                id: 47,
                photoset_order: 6406,
                photoset_id: "72157689697918160",
                title: "Danilo Medina pasa revista a los avances de proyectos desarrollo agroforestal",
                is_public: 1
            },
            {
                id: 48,
                photoset_order: 6405,
                photoset_id: "72157694716575544",
                title: "Presidente encabeza acto de apertura XIX Conferencia Regulación y Supervisión de Seguros",
                is_public: 1
            },
            {
                id: 49,
                photoset_order: 6404,
                photoset_id: "72157694669022844",
                title: "Danilo fomenta exportación mango banilejo. Pequeños productores reciben apoyo para empacadora",
                is_public: 1
            },
            {
                id: 50,
                photoset_order: 6403,
                photoset_id: "72157689617491850",
                title: "Mi familia vive del turismo",
                is_public: 1
            },
            {
                id: 51,
                photoset_order: 6402,
                photoset_id: "72157695386153725",
                title: "Cándida Montilla de Medina recibe distinción en Primer Torneo de Volibol Femenino",
                is_public: 1
            },
            {
                id: 52,
                photoset_order: 6401,
                photoset_id: "72157689611475260",
                title: "No lo para nadie. Democratización del crédito",
                is_public: 1
            },
            {
                id: 53,
                photoset_order: 6400,
                photoset_id: "72157665475699807",
                title: "Reservas del País capacita y entrega equipos tecnológicos a grupos asociativos de Visitas Sorpresa",
                is_public: 1
            },
            {
                id: 54,
                photoset_order: 6399,
                photoset_id: "72157695355690905",
                title: "Reunión Ministro Montalvo",
                is_public: 1
            },
            {
                id: 55,
                photoset_order: 6398,
                photoset_id: "72157692217109202",
                title: "Danilo Medina se reúne con transportistas para tratar conversión sistema transporte pasajeros",
                is_public: 1
            },
            {
                id: 56,
                photoset_order: 6397,
                photoset_id: "72157692216721002",
                title: "Medio Ambiente ordena cierre técnico de siete vertederos",
                is_public: 1
            },
            {
                id: 57,
                photoset_order: 6396,
                photoset_id: "72157689577367810",
                title: "Donación AES Dominicana al 911",
                is_public: 1
            },
            {
                id: 58,
                photoset_order: 6395,
                photoset_id: "72157665462456407",
                title: "Recaudaciones DGII crecen 17% respecto a primer trimestre 2017; revela nueva estrategia antievasión",
                is_public: 1
            },
            {
                id: 59,
                photoset_order: 6394,
                photoset_id: "72157667484914448",
                title: "Presidente Danilo Medina recibe a ejecutivos empresa Huawei",
                is_public: 1
            },
            {
                id: 60,
                photoset_order: 6393,
                photoset_id: "72157667478930268",
                title: "Danilo cumple compromiso; entrega nuevo Hospital Francisco Moscoso Puello",
                is_public: 1
            },
            {
                id: 61,
                photoset_order: 6392,
                photoset_id: "72157694568077974",
                title: "Danilo examina avances metas Gobierno; exhorta a funcionarios reforzar valores ética e integridad",
                is_public: 1
            },
            {
                id: 62,
                photoset_order: 6391,
                photoset_id: "72157689550231190",
                title: "INAP: 19 años fortaleciendo las competencias de los servidores públicos",
                is_public: 1
            },
            {
                id: 63,
                photoset_order: 6390,
                photoset_id: "72157695315460315",
                title: "Pequeños pescadores de Pedernales se reúnen con representantes supermercados",
                is_public: 1
            },
            {
                id: 64,
                photoset_order: 6389,
                photoset_id: "72157667470048248",
                title: "Isa Conde: la minería sostenible puede ser un arma de lucha contra la pobreza",
                is_public: 1
            },
            {
                id: 65,
                photoset_order: 6388,
                photoset_id: "72157689521520390",
                title: "Danilo encabeza desfile cívico-militar y policial conmemoración 174 aniversario Batalla 30 de Marzo",
                is_public: 1
            },
            {
                id: 66,
                photoset_order: 6387,
                photoset_id: "72157667443000208",
                title: "Primera Dama felicita a periodistas por aporte a la democracia, promover la paz y justicia social",
                is_public: 1
            },
            {
                id: 67,
                photoset_order: 6386,
                photoset_id: "72157667437266718",
                title: "Comisión para cuenca del Yaque del Norte (CRYN) avanza en elaboración plan de ordenamiento",
                is_public: 1
            },
            {
                id: 68,
                photoset_order: 6385,
                photoset_id: "72157693450357371",
                title: "Planes y avances de call centers generarán 60 mil empleos bien remunerados",
                is_public: 1
            },
            {
                id: 69,
                photoset_order: 6384,
                photoset_id: "72157689482685340",
                title: "Procurador entrega vehículos a dependencias institución para ofrecer servicios de mayor calidad",
                is_public: 1
            },
            {
                id: 70,
                photoset_order: 6383,
                photoset_id: "72157665365229567",
                title: "Danilo entrega Centro Regional de Capacitación Agrícola y Forestal; el Sur será región clase media",
                is_public: 1
            },
            {
                id: 71,
                photoset_order: 6382,
                photoset_id: "72157692083388032",
                title: "CAID San Juan: Despacho Primera Dama orienta a padres en convivencia hijos con autismo",
                is_public: 1
            },
            {
                id: 72,
                photoset_order: 6381,
                photoset_id: "72157667368894988",
                title: "Villa La Mata se beneficia de transformación servicios de salud; recibe hospital nuevo",
                is_public: 1
            },
            {
                id: 73,
                photoset_order: 6380,
                photoset_id: "72157667363161248",
                title: "COE: medios y ciudadanos logran feliz regreso de más de 4 millones tras Semana Santa",
                is_public: 1
            },
            {
                id: 74,
                photoset_order: 6379,
                photoset_id: "72157693399271261",
                title: "Era un Cuchitril. Hospital Municipal Villa la Mata",
                is_public: 1
            },
            {
                id: 75,
                photoset_order: 6378,
                photoset_id: "72157667356042218",
                title: "Primera Dama exhorta a crear ambiente adecuado y de inserción a personas con autismo",
                is_public: 1
            },
            {
                id: 76,
                photoset_order: 6377,
                photoset_id: "72157694339642834",
                title: "El Coe en un recorrido de supervisión por las playas del este del país",
                is_public: 1
            },
            {
                id: 77,
                photoset_order: 6376,
                photoset_id: "72157665182560147",
                title: "El COE inicia operativo Unión Santa Semana Santa 2018. Proteger vidas humanas en asueto",
                is_public: 1
            },
            {
                id: 78,
                photoset_order: 6375,
                photoset_id: "72157693217454661",
                title: "Cañeros de Higüey reciben más de 4 millones de pesos por tercera liquidación zafra 2016-2017",
                is_public: 1
            },
            {
                id: 79,
                photoset_order: 6374,
                photoset_id: "72157694999911825",
                title: "Banco Agrícola financia tecnología agropecuaria por más de RD$1000 millones",
                is_public: 1
            },
            {
                id: 80,
                photoset_order: 6373,
                photoset_id: "72157691873671822",
                title: "Presidente Danilo Medina se reúne con alcaldes de Barahona para atender sus necesidades",
                is_public: 1
            },
            {
                id: 81,
                photoset_order: 6372,
                photoset_id: "72157667156707148",
                title: "Danilo Medina recibe cartas credenciales embajadores Turquía, Nicaragua, Namibia, Guinea y Chequia",
                is_public: 1
            },
            {
                id: 82,
                photoset_order: 6371,
                photoset_id: "72157689221951920",
                title: "Con Visita Sorpresa 200, Danilo cumple compromiso con los más pobres",
                is_public: 1
            },
            {
                id: 83,
                photoset_order: 6370,
                photoset_id: "72157691840922922",
                title: "Visitas Sorpresa fortalecen asociatividad, titulación de tierras, acceso a agua y dinamizan economía",
                is_public: 1
            },
            {
                id: 84,
                photoset_order: 6369,
                photoset_id: "72157665090769477",
                title: "Despacho Primera Dama respalda por cuarto año jornada de salud en beneficio de unas 6,000 personas",
                is_public: 1
            },
            {
                id: 85,
                photoset_order: 6368,
                photoset_id: "72157693154208031",
                title: "Presidente Danilo Medina escucha necesidades de los veganos",
                is_public: 1
            },
            {
                id: 86,
                photoset_order: 6367,
                photoset_id: "72157665083252037",
                title: "Presidente Danilo Medina recibe a basquetbolista dominicano Ángel Delgado",
                is_public: 1
            },
            {
                id: 87,
                photoset_order: 6366,
                photoset_id: "72157694877890935",
                title: "Danilo arriba a VS 200: más de RD$33 mil millones invertidos, 200 mil empleos y 1,500 proyectos",
                is_public: 1
            },
            {
                id: 88,
                photoset_order: 6365,
                photoset_id: "72157693036870961",
                title: "En centros CAID Santiago y San Juan imparten charlas para concienciar sobre síndrome de Down",
                is_public: 1
            },
            {
                id: 89,
                photoset_order: 6364,
                photoset_id: "72157694060818914",
                title: "Instituto Dominicano de Cardiología moderniza sus instalaciones",
                is_public: 1
            },
            {
                id: 90,
                photoset_order: 6363,
                photoset_id: "72157667016791268",
                title: "Ruta Mipymes llega a Cotuí; emprendedores reciben asesoría, asistencia y capacitación",
                is_public: 1
            },
            {
                id: 91,
                photoset_order: 6362,
                photoset_id: "72157694058130824",
                title: "Dirección General de Ética promueve portal de información para fortalecer el Periodismo de Datos RD",
                is_public: 1
            },
            {
                id: 92,
                photoset_order: 6361,
                photoset_id: "72157691722469922",
                title: "Zoraima Cuello: gobierno de Danilo Medina ha transformado administración pública",
                is_public: 1
            },
            {
                id: 93,
                photoset_order: 6360,
                photoset_id: "72157693030979721",
                title: "Presidente Danilo Medina analiza con funcionarios avances y pendientes según evaluación del GAFILAT",
                is_public: 1
            },
            {
                id: 94,
                photoset_order: 6359,
                photoset_id: "72157694786085525",
                title: "COE anuncia operativo Semana Santa. “Unión Santa por tu Seguridad y los Valores” iniciará jueves 29",
                is_public: 1
            },
            {
                id: 95,
                photoset_order: 6358,
                photoset_id: "72157666992819988",
                title: "Danilo Medina se reúne con empresarios hoteleros de Punta Cana",
                is_public: 1
            },
            {
                id: 96,
                photoset_order: 6357,
                photoset_id: "72157691701020032",
                title: "Año Fomento Exportaciones: Danilo Medina se reúne con Clúster Dispositivos Médicos Zonas Francas",
                is_public: 1
            },
            {
                id: 97,
                photoset_order: 6356,
                photoset_id: "72157693007390551",
                title: "Despacho de la Primera Dama pone en servicio “Farmacia Cuenta Conmigo”; entrega medicinas gratis",
                is_public: 1
            },
            {
                id: 98,
                photoset_order: 6355,
                photoset_id: "72157666985175468",
                title: "RD crece y se democratiza: exportaciones agropecuarias alcanzan 3 mil millones de dólares en 2017",
                is_public: 1
            },
            {
                id: 99,
                photoset_order: 6354,
                photoset_id: "72157693002382871",
                title: "Hato Mayor: Revolución Educativa se consolida. Danilo entrega dos escuelas y edificio administrativo",
                is_public: 1
            },
            {
                id: 100,
                photoset_order: 6353,
                photoset_id: "72157689032713250",
                title: "Avanza transformación urbana y ambiental de El Riito, en La Vega; impactará 225 mil personas",
                is_public: 1
            },
            {
                id: 101,
                photoset_order: 6352,
                photoset_id: "72157689032452600",
                title: "En seguimiento a Visita Sorpresa 199, comisión se reúne con productores y comunitarios de Dajabón",
                is_public: 1
            },
            {
                id: 102,
                photoset_order: 6351,
                photoset_id: "72157664914340527",
                title: "Médicos de Montecristi ya tienen hospital digno para asistir pacientes. Danilo entrega nuevo centro",
                is_public: 1
            },
            {
                id: 103,
                photoset_order: 6350,
                photoset_id: "72157694741716855",
                title: "Gobierno fortalece lazos de cooperación internacional a favor  del Plan Nacional de Titulación",
                is_public: 1
            },
            {
                id: 104,
                photoset_order: 6349,
                photoset_id: "72157664910216657",
                title: "República Digital: 373 estudiantes se forman en desarrollo de software",
                is_public: 1
            },
            {
                id: 105,
                photoset_order: 6348,
                photoset_id: "72157693987926934",
                title: "Flotilla completa autobuses OMSA operarán en mayo con Wi-Fi gratuito; resaltan otras buenas noticias",
                is_public: 1
            },
            {
                id: 106,
                photoset_order: 6347,
                photoset_id: "72157694715709295",
                title: "José Ramón Peralta destaca empeño del Gobierno por eficiencia y transparencia",
                is_public: 1
            },
            {
                id: 107,
                photoset_order: 6346,
                photoset_id: "72157691632163922",
                title: "SeNaSa lanza proyecto piloto para entrega de medicamentos, a través de las Farmacias del Pueblo",
                is_public: 1
            },
            {
                id: 108,
                photoset_order: 6345,
                photoset_id: "72157688994739590",
                title: "Danilo cumple con feligreses de Barahona; entrega Catedral y Obispado completamente restaurados",
                is_public: 1
            },
            {
                id: 109,
                photoset_order: 6344,
                photoset_id: "72157664881094377",
                title: "DPD por Día Mundial del Síndrome de Down",
                is_public: 1
            },
            {
                id: 110,
                photoset_order: 6343,
                photoset_id: "72157692936204781",
                title: "Banca Solidaria colocará este año RD$6,223 millones en manos de más de 120 mil microempresarios",
                is_public: 1
            },
            {
                id: 111,
                photoset_order: 6342,
                photoset_id: "72157694702033175",
                title: "Más empleos e inversiones: Danilo asiste a inauguración cuarta planta dispositivos médicos Medtronic",
                is_public: 1
            },
            {
                id: 112,
                photoset_order: 6341,
                photoset_id: "72157691602458372",
                title: "Presidente Danilo Medina encabeza desfile por 174 aniversario de la Batalla del 19 de Marzo",
                is_public: 1
            },
            {
                id: 113,
                photoset_order: 6340,
                photoset_id: "72157692902531571",
                title: "Ministro Montalvo se reúne con empresarios",
                is_public: 1
            },
            {
                id: 114,
                photoset_order: 6339,
                photoset_id: "72157692900889771",
                title: "Personal CAID recibe entrenamiento para identificar cuando menores son víctimas de violencia",
                is_public: 1
            },
            {
                id: 115,
                photoset_order: 6338,
                photoset_id: "72157692860154791",
                title: "VS199. Danilo respalda proyecto apícola en Dajabón, instruye soluciones para Restauración",
                is_public: 1
            },
            {
                id: 116,
                photoset_order: 6337,
                photoset_id: "72157688886744380",
                title: "Pumarol en programa de Luis Valiente",
                is_public: 1
            },
            {
                id: 117,
                photoset_order: 6336,
                photoset_id: "72157666793344018",
                title: "Quedarás Reyenao. Jóvenes que cambian el mundo",
                is_public: 1
            },
            {
                id: 118,
                photoset_order: 6335,
                photoset_id: "72157693808946424",
                title: "Dulcería Benza. Tireo, Constanza. Somos de Barrio",
                is_public: 1
            },
            {
                id: 119,
                photoset_order: 6334,
                photoset_id: "72157666788426918",
                title: "El Riito, La Vega. Bendición pal pueblo entero",
                is_public: 1
            },
            {
                id: 120,
                photoset_order: 6333,
                photoset_id: "72157693808310994",
                title: "Feria Ganadera: El Sabor de las Visitas Sorpresa",
                is_public: 1
            },
            {
                id: 121,
                photoset_order: 6332,
                photoset_id: "72157692788409321",
                title: "En menos de 48 horas, Danilo entrega dos escuelas más en La Vega; alegría en Pontón y Los Guayos",
                is_public: 1
            },
            {
                id: 122,
                photoset_order: 6331,
                photoset_id: "72157664739291717",
                title: "Inclusión y educación para todos: logros evidentes en programas especiales de la Presidencia en 2017",
                is_public: 1
            },
            {
                id: 123,
                photoset_order: 6330,
                photoset_id: "72157694549646325",
                title: "Call Centers, Zonas Francas. #AñoFomentoExportaciones",
                is_public: 1
            },
            {
                id: 124,
                photoset_order: 6329,
                photoset_id: "72157666779646968",
                title: "Visita de Unicef",
                is_public: 1
            },
            {
                id: 125,
                photoset_order: 6328,
                photoset_id: "72157692784157031",
                title: "Electrónica. Zonas Francas. #AñoFomentoExportaciones",
                is_public: 1
            },
            {
                id: 126,
                photoset_order: 6327,
                photoset_id: "72157666779197108",
                title: "Calidad y Sabor. Lacteos Echavarria. #2018SeráMejor",
                is_public: 1
            },
            {
                id: 127,
                photoset_order: 6326,
                photoset_id: "72157694548895585",
                title: "Calzados, Zonas Francas. #AñoFomentoExportaciones",
                is_public: 1
            },
            {
                id: 128,
                photoset_order: 6325,
                photoset_id: "72157694548057725",
                title: "Joyería. Zonas Francas. #AñoFomentoExportaciones",
                is_public: 1
            },
            {
                id: 129,
                photoset_order: 6324,
                photoset_id: "72157666778272758",
                title: "Dispositivos Médicos, Zonas Francas. #AñoFomentoExportaciones",
                is_public: 1
            },
            {
                id: 130,
                photoset_order: 6323,
                photoset_id: "72157666778135618",
                title: "Tabaco, Zonas Francas. #AñoFomentoExportaciones",
                is_public: 1
            },
            {
                id: 131,
                photoset_order: 6322,
                photoset_id: "72157664735351167",
                title: "Textil. Zonas Francas. #AñoFomentoExportaciones",
                is_public: 1
            },
            {
                id: 132,
                photoset_order: 6321,
                photoset_id: "72157688847759060",
                title: "Captando clientes. Hoover Chacabanas",
                is_public: 1
            },
            {
                id: 133,
                photoset_order: 6320,
                photoset_id: "72157664734829217",
                title: "Mejor producción: Asogracruz",
                is_public: 1
            },
            {
                id: 134,
                photoset_order: 6319,
                photoset_id: "72157664734608867",
                title: "Juan Pumarol: Visitas Sorpresa son estrategia de desarrollo de sectores productivos",
                is_public: 1
            },
            {
                id: 135,
                photoset_order: 6318,
                photoset_id: "72157691483610662",
                title: "Fortalecer ventas. Empresa María #2018SeráMejor",
                is_public: 1
            },
            {
                id: 136,
                photoset_order: 6317,
                photoset_id: "72157688847003600",
                title: "Lleno de éxitos. Wild Play #2018SeráMejor",
                is_public: 1
            },
            {
                id: 137,
                photoset_order: 6316,
                photoset_id: "72157664733274777",
                title: "Cero fiao. Modesto Fresh.#2018SeráMejor",
                is_public: 1
            },
            {
                id: 138,
                photoset_order: 6315,
                photoset_id: "72157664733169487",
                title: "Con mejor sueldo. Hospital Haina. #2018SeráMejor",
                is_public: 1
            },
            {
                id: 139,
                photoset_order: 6314,
                photoset_id: "72157693778791984",
                title: "Primera Dama coordina escogencia de 1,017 voluntarios acompañarán atletas Olimpiadas Especiales",
                is_public: 1
            },
            {
                id: 140,
                photoset_order: 6313,
                photoset_id: "72157666757111508",
                title: "Danilo Medina recibe Primer Ministro Corea del Sur. Se fortalecen lazos entre ambos países",
                is_public: 1
            },
            {
                id: 141,
                photoset_order: 6312,
                photoset_id: "72157694521303835",
                title: "Viviendas dignas para todos; funcionarios del sector rinden cuentas de su gestión en 2017",
                is_public: 1
            },
            {
                id: 142,
                photoset_order: 6311,
                photoset_id: "72157693768810924",
                title: "Pro Consumidor celebra Día Mundial Derechos Consumidor; promueve buenas prácticas mercados digitales",
                is_public: 1
            },
            {
                id: 143,
                photoset_order: 6310,
                photoset_id: "72157664704623807",
                title: "De más éxito. D`Chiringuito Restaurant",
                is_public: 1
            },
            {
                id: 144,
                photoset_order: 6309,
                photoset_id: "72157693762328134",
                title: "Y de paso, un nieto. Colmado Onésimo #2018SeraMejor",
                is_public: 1
            },
            {
                id: 145,
                photoset_order: 6308,
                photoset_id: "72157694496489735",
                title: "Danilo Medina recibe empresarios asiáticos interesados en desarrollar en RD proyectos textiles",
                is_public: 1
            },
            {
                id: 146,
                photoset_order: 6307,
                photoset_id: "72157693745788254",
                title: "Gobierno lanza programa educación técnico profesional con apoyo Unión Europea y Cooperación Española",
                is_public: 1
            },
            {
                id: 147,
                photoset_order: 6306,
                photoset_id: "72157691435920172",
                title: "Comodidad para estudiantes de Constanza, tras recibir escuela; Danilo entrega otra en La Vega",
                is_public: 1
            },
            {
                id: 148,
                photoset_order: 6305,
                photoset_id: "72157693741625574",
                title: "CONADIS anuncia inicio postulación del Sello de Buenas Prácticas RD Incluye 2018",
                is_public: 1
            },
            {
                id: 149,
                photoset_order: 6304,
                photoset_id: "72157692728776951",
                title: "Ministro Gustavo Montalvo recibe canciller de  España",
                is_public: 1
            },
            {
                id: 150,
                photoset_order: 6303,
                photoset_id: "72157694486101115",
                title: "Danilo Medina recibe al Ministro de Asuntos Exteriores y Cooperación de España, Alfonso Dastis",
                is_public: 1
            },
            {
                id: 151,
                photoset_order: 6302,
                photoset_id: "72157666720622208",
                title: "Instituciones gubernamentales destacan transparencia sin precedentes en 2017",
                is_public: 1
            },
            {
                id: 152,
                photoset_order: 6301,
                photoset_id: "72157666714966998",
                title: "Danilo Medina escucha informe sobre avances del sector eléctrico",
                is_public: 1
            },
            {
                id: 153,
                photoset_order: 6300,
                photoset_id: "72157666701366778",
                title: "Macaraos de Salcedo. Somos Carnaval",
                is_public: 1
            },
            {
                id: 154,
                photoset_order: 6299,
                photoset_id: "72157692694468111",
                title: "Alegría en Duvergé; Primera Dama entrega 21 viviendas, remodelación de capilla y asfaltado de calles",
                is_public: 1
            },
            {
                id: 155,
                photoset_order: 6298,
                photoset_id: "72157688761894200",
                title: "Danilo Medina cumple con los comunitarios de Polo, les entrega hospital remozado y equipado",
                is_public: 1
            },
            {
                id: 156,
                photoset_order: 6297,
                photoset_id: "72157693700071614",
                title: "Grandes avances en salud a favor de la gente; titulares instituciones rinden cuentas 2017",
                is_public: 1
            },
            {
                id: 157,
                photoset_order: 6296,
                photoset_id: "72157664644779227",
                title: "Presidente revisa avances en transporte y movilidad",
                is_public: 1
            },
            {
                id: 158,
                photoset_order: 6295,
                photoset_id: "72157688755131560",
                title: "IAD reafirma propiedad de terrenos en Dajabón; lleva a tribunales compañía Biofcacao​",
                is_public: 1
            },
            {
                id: 159,
                photoset_order: 6294,
                photoset_id: "72157664641258707",
                title: "Reunion despacho Presidencial Transporte",
                is_public: 1
            },
            {
                id: 160,
                photoset_order: 6293,
                photoset_id: "72157664617595517",
                title: "Padres, estudiantes y profesores de Villa Tropicalia reciben con alegría su nueva escuela",
                is_public: 1
            },
            {
                id: 161,
                photoset_order: 6292,
                photoset_id: "72157688727002040",
                title: "Asesor de OPS-OMS destaca que el CAID se alinea a la Estrategia Global de Rehabilitación de la OMS",
                is_public: 1
            },
            {
                id: 162,
                photoset_order: 6291,
                photoset_id: "72157688724875240",
                title: "Presidente pasa balance sobre avances en sector salud",
                is_public: 1
            },
            {
                id: 163,
                photoset_order: 6290,
                photoset_id: "72157694411793635",
                title: "Reunión Despacho Presidencial 1",
                is_public: 1
            },
            {
                id: 164,
                photoset_order: 6289,
                photoset_id: "72157666653025068",
                title: "Sector agropecuario rinde cuentas al país de su gestión en 2017; crece 5.9%",
                is_public: 1
            },
            {
                id: 165,
                photoset_order: 6288,
                photoset_id: "72157692645401191",
                title: "Juan Pumarol dice Visitas Sorpresa continúan y serán más",
                is_public: 1
            },
            {
                id: 166,
                photoset_order: 6287,
                photoset_id: "72157694378113825",
                title: "VS198: Danilo va a Tierra Nueva, Jimaní y Duvergé. Trabaja para que la gente no abandone la frontera",
                is_public: 1
            },
            {
                id: 167,
                photoset_order: 6286,
                photoset_id: "72157688690201170",
                title: "Espartillar, Duvergé, promesa cumplida",
                is_public: 1
            },
            {
                id: 168,
                photoset_order: 6285,
                photoset_id: "72157688689553920",
                title: "Impacto de Amber Cove en Puerto Plata",
                is_public: 1
            },
            {
                id: 169,
                photoset_order: 6284,
                photoset_id: "72157693593096724",
                title: "Presidente conoce avances de la Estación Depuradora de Aguas Residuales del río Ozama",
                is_public: 1
            },
            {
                id: 170,
                photoset_order: 6283,
                photoset_id: "72157694345521455",
                title: "Danilo vuelve a ProBambú en Juma, Bonao",
                is_public: 1
            },
            {
                id: 171,
                photoset_order: 6282,
                photoset_id: "72157688656474280",
                title: "Califé de la capital. Somos carnaval",
                is_public: 1
            },
            {
                id: 172,
                photoset_order: 6281,
                photoset_id: "72157666575981168",
                title: "Trabajamos como hormiguitas. Siempre mujeres",
                is_public: 1
            },
            {
                id: 173,
                photoset_order: 6280,
                photoset_id: "72157693552264824",
                title: "Danilo Medina supervisa construcción CAID Santo Domingo Este",
                is_public: 1
            },
            {
                id: 174,
                photoset_order: 6279,
                photoset_id: "72157688618557240",
                title: "Metro de Santo Domingo orienta estudiantes; promueve “Cultura Metro”",
                is_public: 1
            },
            {
                id: 175,
                photoset_order: 6278,
                photoset_id: "72157694293941805",
                title: "A la mujer le gusta estar bella. Siempre Mujeres",
                is_public: 1
            },
            {
                id: 176,
                photoset_order: 6277,
                photoset_id: "72157693540163194",
                title: "Equipamiento Hospital Provincial Padre Fantino en Montecristi",
                is_public: 1
            },
            {
                id: 177,
                photoset_order: 6276,
                photoset_id: "72157688615491970",
                title: "Reunión delegación del PNUD",
                is_public: 1
            },
            {
                id: 178,
                photoset_order: 6275,
                photoset_id: "72157693539534844",
                title: "Comisión presidencial se reúne con productores de especias de Pedro Brand; coordinan trabajos",
                is_public: 1
            },
            {
                id: 179,
                photoset_order: 6274,
                photoset_id: "72157666542603568",
                title: "Gobierno desarrolla propuesta para la reinserción escolar de jóvenes y adultos",
                is_public: 1
            },
            {
                id: 180,
                photoset_order: 6273,
                photoset_id: "72157688614995910",
                title: "Visitas Sorpresa buscan soluciones a los desafíos del sector agropecuario",
                is_public: 1
            },
            {
                id: 181,
                photoset_order: 6272,
                photoset_id: "72157694267415925",
                title: "En Los Hidalgos ya hay servicios de salud humanizados y de calidad; Danilo entrega hospital remozado",
                is_public: 1
            },
            {
                id: 182,
                photoset_order: 6271,
                photoset_id: "72157691223139812",
                title: "Con entusiasmo, orgullo y alegría, Danilo entrega Medalla al Mérito a 13 mujeres dominicanas",
                is_public: 1
            },
            {
                id: 183,
                photoset_order: 6270,
                photoset_id: "72157688584057920",
                title: "RRHH del MIMPRE ",
                is_public: 1
            },
            {
                id: 184,
                photoset_order: 6269,
                photoset_id: "72157691218798432",
                title: "Mujeres desempeñan un rol fundamental en proyectos de las Visitas Sorpresa",
                is_public: 1
            },
            {
                id: 185,
                photoset_order: 6268,
                photoset_id: "72157664456719607",
                title: "El gobierno de Danilo es honesto y democratizador; no hace “bulto”",
                is_public: 1
            },
            {
                id: 186,
                photoset_order: 6267,
                photoset_id: "72157666494885018",
                title: "Rueda de Prensa Competitividad",
                is_public: 1
            },
            {
                id: 187,
                photoset_order: 6266,
                photoset_id: "72157692482442301",
                title: "Gobierno y PNUD presentan estrategia para impulsar Desarrollo Sostenible y eliminar la pobreza",
                is_public: 1
            },
            {
                id: 188,
                photoset_order: 6265,
                photoset_id: "72157664449594147",
                title: "Danilo Medina emite decreto para otorgamiento renovaciones automáticas de registros sanitarios",
                is_public: 1
            },
            {
                id: 189,
                photoset_order: 6264,
                photoset_id: "72157666487145308",
                title: "Cándida Montilla de Medina exhorta a la mujer dominicana continuar luchando por la igualdad social",
                is_public: 1
            },
            {
                id: 190,
                photoset_order: 6263,
                photoset_id: "72157693477982724",
                title: "Tras recorrido de Danilo Medina por Barahona y Bahoruco, IAD formaliza acuerdos con productores   Previous  Next",
                is_public: 1
            },
            {
                id: 191,
                photoset_order: 6262,
                photoset_id: "72157692476486481",
                title: "JR Peralta: búsqueda nuevos mercados y productos exportación, el mejor camino para crear más empleos",
                is_public: 1
            },
            {
                id: 192,
                photoset_order: 6261,
                photoset_id: "72157664443532747",
                title: "Cientos de personas asisten a visitas guiadas al Teleférico de Santo Domingo; valoran servicio",
                is_public: 1
            },
            {
                id: 193,
                photoset_order: 6260,
                photoset_id: "72157691191031572",
                title: "En PDAC 2018, RD promueve industria extractiva sostenible; se interesa en formación técnica minera",
                is_public: 1
            },
            {
                id: 194,
                photoset_order: 6259,
                photoset_id: "72157664440717987",
                title: "Revolucionamos la salud en Los Hidalgos",
                is_public: 1
            },
            {
                id: 195,
                photoset_order: 6258,
                photoset_id: "72157693470165414",
                title: "Gobierno incrementa protección de la frontera por aire, mar y tierra",
                is_public: 1
            },
            {
                id: 196,
                photoset_order: 6257,
                photoset_id: "72157693468811904",
                title: "Economía dominicana continuará creciendo",
                is_public: 1
            },
            {
                id: 197,
                photoset_order: 6256,
                photoset_id: "72157694223148275",
                title: "Diablos Cajuelos de la Capital. Somos Carnaval",
                is_public: 1
            },
            {
                id: 198,
                photoset_order: 6255,
                photoset_id: "72157688551174350",
                title: "Zoraima Cuello: para Danilo no hay distancias que impidan aplicar y enseñar las nuevas tecnologías",
                is_public: 1
            },
            {
                id: 199,
                photoset_order: 6254,
                photoset_id: "72157694219747345",
                title: "Entrevista encargados departamentales 911 norte",
                is_public: 1
            },
            {
                id: 200,
                photoset_order: 6253,
                photoset_id: "72157688528992290",
                title: "Comisión Presidencial Cuenca Yaque del Norte acuerda metodología para Plan Estratégico",
                is_public: 1
            },
            {
                id: 201,
                photoset_order: 6252,
                photoset_id: "72157691162539342",
                title: "Barahona: Danilo Medina entrega Unidad de Atención Violencia de Género e Intrafamiliar",
                is_public: 1
            },
            {
                id: 202,
                photoset_order: 6251,
                photoset_id: "72157664411332097",
                title: "Tejada Holguín: República Dominicana está cambiando; gobierno busca resolver problemas estructurales",
                is_public: 1
            },
            {
                id: 203,
                photoset_order: 6250,
                photoset_id: "72157666450814478",
                title: "Parque Solar Canoa abastecerá de luz a 10 mil hogares",
                is_public: 1
            },
            {
                id: 204,
                photoset_order: 6249,
                photoset_id: "72157664405434907",
                title: "Hay que organizarse",
                is_public: 1
            },
            {
                id: 205,
                photoset_order: 6248,
                photoset_id: "72157666444964628",
                title: "En Neiba, Danilo entrega Centro de Diagnóstico y deja iniciada construcción de hospital",
                is_public: 1
            },
            {
                id: 206,
                photoset_order: 6247,
                photoset_id: "72157694185029825",
                title: "¿Porque, cual es la diferiencia? Siempre mujeres",
                is_public: 1
            },
            {
                id: 207,
                photoset_order: 6246,
                photoset_id: "72157694165441715",
                title: "Primera Dama presenta avance montaje de Olimpiadas Especiales a celebrarse en RD",
                is_public: 1
            },
            {
                id: 208,
                photoset_order: 6245,
                photoset_id: "72157666423491938",
                title: "Danilo Medina entrega sede República Digital Educación para elevar calidad enseñanza aprendizaje",
                is_public: 1
            },
            {
                id: 209,
                photoset_order: 6244,
                photoset_id: "72157688493607800",
                title: "Construcción del Hotel Lifestyle Cabarete",
                is_public: 1
            },
            {
                id: 210,
                photoset_order: 6243,
                photoset_id: "72157692408831011",
                title: "Carnamar de Río San Juan. Somos Carnaval",
                is_public: 1
            },
            {
                id: 211,
                photoset_order: 6242,
                photoset_id: "72157693394765344",
                title: "Platanuses, Papeluses y Funduses de Cotuí. Somos Carnaval",
                is_public: 1
            },
            {
                id: 212,
                photoset_order: 6241,
                photoset_id: "72157694150304225",
                title: "Damiselas de Cotuí. Somos Carnaval",
                is_public: 1
            },
            {
                id: 213,
                photoset_order: 6240,
                photoset_id: "72157692397985801",
                title: "Si yo cambio, cambia el mundo",
                is_public: 1
            },
            {
                id: 214,
                photoset_order: 6239,
                photoset_id: "72157688480454690",
                title: "Entrevista a Roberto Rodriguez Marchena en Hoy Mismo",
                is_public: 1
            },
            {
                id: 215,
                photoset_order: 6238,
                photoset_id: "72157664340915627",
                title: "VS 197. Danilo Medina ofrece apoyo a productores de especias de La Cuaba, Pedro Brand",
                is_public: 1
            },
            {
                id: 216,
                photoset_order: 6237,
                photoset_id: "72157688453986970",
                title: "Mejor servicio y confort. D’Caballeros Barber Shop. #2018SeráMejor",
                is_public: 1
            },
            {
                id: 217,
                photoset_order: 6236,
                photoset_id: "72157664314533457",
                title: "Danilo recorre Granado, Cabeza de Toro y Presa Monte Grande; evalúa impacto financiamiento y obras",
                is_public: 1
            },
            {
                id: 218,
                photoset_order: 6235,
                photoset_id: "72157666349234538",
                title: "Juampa de Cotuí. Somos Carnaval",
                is_public: 1
            },
            {
                id: 219,
                photoset_order: 6234,
                photoset_id: "72157694076349795",
                title: "Museo del Carnaval Vegano. Somos Carnaval",
                is_public: 1
            },
            {
                id: 220,
                photoset_order: 6233,
                photoset_id: "72157694047916685",
                title: "Despacho de la Primera Dama coordina la creación del primer Registro Nacional del Cáncer",
                is_public: 1
            },
            {
                id: 221,
                photoset_order: 6232,
                photoset_id: "72157688380666840",
                title: "Funcionarios destacan avances en la agropecuaria nacional",
                is_public: 1
            },
            {
                id: 222,
                photoset_order: 6231,
                photoset_id: "72157666305079508",
                title: "Danilo da primer palazo Hotel Lopesan Costa Bávaro de 1,025 habitaciones",
                is_public: 1
            },
            {
                id: 223,
                photoset_order: 6230,
                photoset_id: "72157692289709001",
                title: "IAD",
                is_public: 1
            },
            {
                id: 224,
                photoset_order: 6229,
                photoset_id: "72157688377548930",
                title: "CAASD",
                is_public: 1
            },
            {
                id: 225,
                photoset_order: 6228,
                photoset_id: "72157694018834875",
                title: "Nueva etapa en recuperación Valle Nuevo: Familias son trasladadas a casas propias y dignas",
                is_public: 1
            },
            {
                id: 226,
                photoset_order: 6227,
                photoset_id: "72157692266893681",
                title: "A raíz de Visita Sorpresa 155, Gobierno inicia trabajos de titulación parceleros de El Seibo",
                is_public: 1
            },
            {
                id: 227,
                photoset_order: 6226,
                photoset_id: "72157664238102067",
                title: "Equipamiento Hosp. Dr. Alberto Gautreaux. Sánchez, Samaná.",
                is_public: 1
            },
            {
                id: 228,
                photoset_order: 6225,
                photoset_id: "72157688349093490",
                title: "Promesa cumplida: Hondo Valle recibe Hospital Municipal remozado, ampliado y equipado",
                is_public: 1
            },
            {
                id: 229,
                photoset_order: 6224,
                photoset_id: "72157688342170960",
                title: "Juan Pumarol valora discurso de Danilo Medina como honesto, completo, inspirador y cargado de hechos",
                is_public: 1
            },
            {
                id: 230,
                photoset_order: 6223,
                photoset_id: "72157664228298677",
                title: "Para todo el que lo necesite. Hospital Municipal de Hondo Valle",
                is_public: 1
            },
            {
                id: 231,
                photoset_order: 6222,
                photoset_id: "72157692252057691",
                title: "El viejo Hospital Municipal Hondo Valle",
                is_public: 1
            },
            {
                id: 232,
                photoset_order: 6221,
                photoset_id: "72157666268249848",
                title: "Guloyas de San Pedro de Macorís. Somos Carnaval",
                is_public: 1
            },
            {
                id: 233,
                photoset_order: 6220,
                photoset_id: "72157666268179608",
                title: "Gobierno rinde cuenta todos los días. Democratización es mejor forma de luchar contra corrupción",
                is_public: 1
            },
            {
                id: 234,
                photoset_order: 6219,
                photoset_id: "72157664211390797",
                title: "Discurso de Danilo Medina confirma revolución de oportunidades durante su Gobierno",
                is_public: 1
            },
            {
                id: 235,
                photoset_order: 6218,
                photoset_id: "72157692231843471",
                title: "En cumplimiento a VS 184: parceleros de Baní reciben 1,281 títulos definitivos",
                is_public: 1
            },
            {
                id: 236,
                photoset_order: 6217,
                photoset_id: "72157690954578742",
                title: "Dominicanos residentes en Boston adquieren 45 viviendas en Ciudad Juan Bosch",
                is_public: 1
            },
            {
                id: 237,
                photoset_order: 6216,
                photoset_id: "72157688313689680",
                title: "Reunión en el despacho Presidencial",
                is_public: 1
            },
            {
                id: 238,
                photoset_order: 6215,
                photoset_id: "72157693963052605",
                title: "Tejada Holguín: Discurso de Danilo explica proyecto de nación por el que debemos marchar juntos",
                is_public: 1
            },
            {
                id: 239,
                photoset_order: 6214,
                photoset_id: "72157693961767395",
                title: "Zoraima Cuello: República Digital uno de los proyectos más innovadores del país",
                is_public: 1
            },
            {
                id: 240,
                photoset_order: 6213,
                photoset_id: "72157690943973832",
                title: "Tabacalera Caoba",
                is_public: 1
            },
            {
                id: 241,
                photoset_order: 6212,
                photoset_id: "72157664194046077",
                title: "Entrevista a Roberto Rodríguez Marchena en Matinal 5, Telemicro",
                is_public: 1
            },
            {
                id: 242,
                photoset_order: 6211,
                photoset_id: "72157693196312884",
                title: "Brujos de San Juan. Somos Carnaval",
                is_public: 1
            },
            {
                id: 243,
                photoset_order: 6210,
                photoset_id: "72157688290222670",
                title: "Roberto Rodríguez Marchena: “Discurso de Danilo Medina fue pedagógico, explicó los porqués-",
                is_public: 1
            },
            {
                id: 244,
                photoset_order: 6209,
                photoset_id: "72157693938408095",
                title: "Danilo Medina preside desfile militar por 174 aniversario de la Independencia Nacional",
                is_public: 1
            },
            {
                id: 245,
                photoset_order: 6208,
                photoset_id: "72157693170333004",
                title: "En 174 aniversario de la Independencia Nacional, Presidente rinde tributo a los Padres de la Patria",
                is_public: 1
            },
            {
                id: 246,
                photoset_order: 6207,
                photoset_id: "72157692189973341",
                title: "Al conmemorarse 174 aniversario de la Independencia Nacional, Danilo Medina participa en tedeum",
                is_public: 1
            },
            {
                id: 247,
                photoset_order: 6206,
                photoset_id: "72157693926210775",
                title: "Danilo Medina en rendición cuentas al pueblo dominicano #Danilo27F",
                is_public: 1
            },
            {
                id: 248,
                photoset_order: 6205,
                photoset_id: "72157692149480901",
                title: "Más de 22 mil personas afectadas por cataratas reciben cobertura del SeNaSa",
                is_public: 1
            },
            {
                id: 249,
                photoset_order: 6204,
                photoset_id: "72157664128688327",
                title: "Niños y niñas del CAID se gradúan del programa Habilidades Sociales",
                is_public: 1
            },
            {
                id: 250,
                photoset_order: 6203,
                photoset_id: "72157693884399115",
                title: "MAPRE es primera institución pública certificada como “Mejor lugar para trabajar” del Caribe",
                is_public: 1
            },
            {
                id: 251,
                photoset_order: 6202,
                photoset_id: "72157688235478060",
                title: "Presidente encabeza homenaje a la Bandera Nacional",
                is_public: 1
            },
            {
                id: 252,
                photoset_order: 6201,
                photoset_id: "72157664030147767",
                title: "Gobierno proyecta entrada en operación de más de 20 centros Mipymes",
                is_public: 1
            },
            {
                id: 253,
                photoset_order: 6200,
                photoset_id: "72157693780451995",
                title: "Más facilidades para el ciudadano; CAASD firma Carta Compromiso",
                is_public: 1
            },
            {
                id: 254,
                photoset_order: 6199,
                photoset_id: "72157692039570801",
                title: "Punta Catalina avanzada en más 85%; directivos de BID la visitan",
                is_public: 1
            },
            {
                id: 255,
                photoset_order: 6198,
                photoset_id: "72157693001827034",
                title: "Danilo Medina: “Teleférico de Santo Domingo es obra de conectividad para la gente”",
                is_public: 1
            },
            {
                id: 256,
                photoset_order: 6197,
                photoset_id: "72157688129949720",
                title: "Fundación Dominicana de Cardiología reconoce a Gustavo Montalvo por su apoyo",
                is_public: 1
            },
            {
                id: 257,
                photoset_order: 6196,
                photoset_id: "72157690747498982",
                title: "En cumplimiento a VS 178, Gobierno inicia construcción de dos comedores en Independencia y Bahoruco",
                is_public: 1
            },
            {
                id: 258,
                photoset_order: 6195,
                photoset_id: "72157688109578420",
                title: "Comisión del BID elogia labor del Sistema 9-1-1 al servicio de la ciudadanía",
                is_public: 1
            },
            {
                id: 259,
                photoset_order: 6194,
                photoset_id: "72157693741714775",
                title: "Presidente Danilo Medina entrega Escuela de Hotelería, Gastronomía y Pastelería, en Higüey",
                is_public: 1
            },
            {
                id: 260,
                photoset_order: 6193,
                photoset_id: "72157666030386988",
                title: "Productores de Vicente Noble se reúnen con comisión presidencial para coordinar iniciativas VS 196",
                is_public: 1
            },
            {
                id: 261,
                photoset_order: 6192,
                photoset_id: "72157693737930615",
                title: "Vicente Noble. La cosa va caminando",
                is_public: 1
            },
            {
                id: 262,
                photoset_order: 6191,
                photoset_id: "72157692970370654",
                title: "Cándida Montilla de Medina asiste a tradicional Cena Pan y Vino",
                is_public: 1
            },
            {
                id: 263,
                photoset_order: 6190,
                photoset_id: "72157692967301954",
                title: "Danilo responde a reclamo de más de 20 años; entrega carretera Padre Las Casas-Guayabal y puente",
                is_public: 1
            },
            {
                id: 264,
                photoset_order: 6189,
                photoset_id: "72157688094778030",
                title: "Directora OPS resalta logros Sistema 9-1-1 en visita a sede Santo Domingo",
                is_public: 1
            },
            {
                id: 265,
                photoset_order: 6188,
                photoset_id: "72157690712397272",
                title: "Clausura Seminario",
                is_public: 1
            },
            {
                id: 266,
                photoset_order: 6187,
                photoset_id: "72157691969652691",
                title: "Directivos del BID visitan el CAIPI La Malena, Higüey; replicarían modelo en sus países",
                is_public: 1
            },
            {
                id: 267,
                photoset_order: 6186,
                photoset_id: "72157692938479814",
                title: "Nordeste, región de propietarios: productores de Arenoso y Villa Riva reciben 1,304 títulos",
                is_public: 1
            },
            {
                id: 268,
                photoset_order: 6185,
                photoset_id: "72157691968293551",
                title: "Tener un título es una alegría",
                is_public: 1
            },
            {
                id: 269,
                photoset_order: 6184,
                photoset_id: "72157691966143731",
                title: "Ya puedo hablar duro",
                is_public: 1
            },
            {
                id: 270,
                photoset_order: 6183,
                photoset_id: "72157692932859784",
                title: "Gobierno y FAO firman Marco de Programación de País 2018-2021",
                is_public: 1
            },
            {
                id: 271,
                photoset_order: 6182,
                photoset_id: "72157688064796820",
                title: "UNICARIBE. Reunión Ministro Montalvo.",
                is_public: 1
            },
            {
                id: 272,
                photoset_order: 6181,
                photoset_id: "72157691958041321",
                title: "Salud: grandes avances y retos. OPS apoyará sistema de salud pública y 9-1-1",
                is_public: 1
            },
            {
                id: 273,
                photoset_order: 6180,
                photoset_id: "72157665967744628",
                title: "Presidente Danilo Medina juramenta funcionarios designados ayer a través de decretos",
                is_public: 1
            },
            {
                id: 274,
                photoset_order: 6179,
                photoset_id: "72157692900053574",
                title: "Presidente juramenta la Comisión para el Ordenamiento y Manejo de Cuenca del Río Yaque del Norte",
                is_public: 1
            },
            {
                id: 275,
                photoset_order: 6178,
                photoset_id: "72157691932736501",
                title: "Compromiso cumplido: Gobierno entrega tractor y rastra a productores de Las Calderas",
                is_public: 1
            },
            {
                id: 276,
                photoset_order: 6177,
                photoset_id: "72157693667744715",
                title: "Tres mil personas de Bahoruco reciben atención médica en jornada del Despacho Primera Dama",
                is_public: 1
            },
            {
                id: 277,
                photoset_order: 6176,
                photoset_id: "72157688033070170",
                title: "Danilo Medina entrega Parque Central de Santiago, pulmón ecológico y espacio deportivo y recreativo",
                is_public: 1
            },
            {
                id: 278,
                photoset_order: 6175,
                photoset_id: "72157665960232768",
                title: "BID, gran aliado de República Dominicana, da apertura a XXXII Reunión de Gobernadores en Punta Cana",
                is_public: 1
            },
            {
                id: 279,
                photoset_order: 6174,
                photoset_id: "72157692883250864",
                title: "Reunión transporte. Ministro Montalvo",
                is_public: 1
            },
            {
                id: 280,
                photoset_order: 6173,
                photoset_id: "72157691896997161",
                title: "Realidades, paradigmas y desafíos de la integración, nuevo seminario",
                is_public: 1
            },
            {
                id: 281,
                photoset_order: 6172,
                photoset_id: "72157663880362957",
                title: "-El Presidente nos está tomando en cuenta-. Danilo entrega tres planteles en Puerto Plata",
                is_public: 1
            },
            {
                id: 282,
                photoset_order: 6171,
                photoset_id: "72157687989521650",
                title: "Presidente se reúne con miembros del Consejo Regional de Desarrollo",
                is_public: 1
            },
            {
                id: 283,
                photoset_order: 6170,
                photoset_id: "72157692843213884",
                title: "Entrega Planta Procesadora De Alimentos En El Factor, Nagua",
                is_public: 1
            },
            {
                id: 284,
                photoset_order: 6169,
                photoset_id: "72157687986054240",
                title: "Frabricar 1 millón de piezas. Interiores Walmas",
                is_public: 1
            },
            {
                id: 285,
                photoset_order: 6168,
                photoset_id: "72157687985782040",
                title: "Calidad y mejor precio. La Grasa restaurante #2018SeráMejor",
                is_public: 1
            },
            {
                id: 286,
                photoset_order: 6167,
                photoset_id: "72157663871206497",
                title: "Triplicar Producción. Sosa Manufacture #2018SeráMejor",
                is_public: 1
            },
            {
                id: 287,
                photoset_order: 6166,
                photoset_id: "72157665913266288",
                title: "Más variedad. Alexis Empanadas #2018SeráMejor",
                is_public: 1
            },
            {
                id: 288,
                photoset_order: 6165,
                photoset_id: "72157693608843175",
                title: "Nuevos equipos. Team Personalizados RD #2018SeráMejor",
                is_public: 1
            },
            {
                id: 289,
                photoset_order: 6164,
                photoset_id: "72157692840230354",
                title: "Nuevas técnicas. El Guardia Barber Shop #2018SeráMejor",
                is_public: 1
            },
            {
                id: 290,
                photoset_order: 6163,
                photoset_id: "72157693579457145",
                title: "Gobierno y organismos internacionales avanzan en desarrollo agenda inclusión social",
                is_public: 1
            },
            {
                id: 291,
                photoset_order: 6162,
                photoset_id: "72157687955105720",
                title: "La Primera Dama en concierto de Quiéreme como Soy: Es un mensaje de amor, solidaridad y unión familiar",
                is_public: 1
            },
            {
                id: 292,
                photoset_order: 6161,
                photoset_id: "72157663830361937",
                title: "VS196. Danilo lleva apoyo productores pitahaya Vicente Noble. Garantiza convertirlos en clase media",
                is_public: 1
            },
            {
                id: 293,
                photoset_order: 6160,
                photoset_id: "72157692771625154",
                title: "En Sánchez Ramírez, Danilo supervisa avances construcción planta de casabe y procesadora frutas",
                is_public: 1
            },
            {
                id: 294,
                photoset_order: 6159,
                photoset_id: "72157692731197894",
                title: "470 mil habitantes Boca Chica y alrededores se beneficiarán con rehabilitación sistema de pozos",
                is_public: 1
            },
            {
                id: 295,
                photoset_order: 6158,
                photoset_id: "72157665810734178",
                title: "Gobierno realizará seminario internacional “Realidades, paradigmas y desafíos de la integración”",
                is_public: 1
            },
            {
                id: 296,
                photoset_order: 6157,
                photoset_id: "72157687881823150",
                title: "Visita Prensa EFE a instalaciones 9-1-1",
                is_public: 1
            },
            {
                id: 297,
                photoset_order: 6156,
                photoset_id: "72157693490826465",
                title: "Somos Carnaval. Los Pintaos de Barahona",
                is_public: 1
            },
            {
                id: 298,
                photoset_order: 6155,
                photoset_id: "72157687877566920",
                title: "Dagoberto Tejeda. Somos carnaval",
                is_public: 1
            },
            {
                id: 299,
                photoset_order: 6154,
                photoset_id: "72157690509960192",
                title: "Somos Carnaval. Roba la gallina",
                is_public: 1
            },
            {
                id: 300,
                photoset_order: 6153,
                photoset_id: "72157690509904982",
                title: "Somos Carnaval. Taimáscaros de Puerto Plata",
                is_public: 1
            },
            {
                id: 301,
                photoset_order: 6152,
                photoset_id: "72157693489709385",
                title: "Somos Carnaval. Diablos cojuelos de La Vega",
                is_public: 1
            },
            {
                id: 302,
                photoset_order: 6151,
                photoset_id: "72157665803221288",
                title: "Somos Carnaval. Tiznaos de la Capital",
                is_public: 1
            },
            {
                id: 303,
                photoset_order: 6150,
                photoset_id: "72157692719046834",
                title: "Somos carnaval. Los Lechones de Santiago",
                is_public: 1
            },
            {
                id: 304,
                photoset_order: 6149,
                photoset_id: "72157665782129768",
                title: "Compromiso cumplido: El Factor recibe procesadora de alimentos agregará valor a productos nacionales",
                is_public: 1
            },
            {
                id: 305,
                photoset_order: 6148,
                photoset_id: "72157692687121374",
                title: "República Digital lanza nuevos servicios e inaugura Datacenter del Estado",
                is_public: 1
            },
            {
                id: 306,
                photoset_order: 6147,
                photoset_id: "72157665768670908",
                title: "Presidente Danilo Medina recibe delegación de ministros de Dinamarca",
                is_public: 1
            },
            {
                id: 307,
                photoset_order: 6146,
                photoset_id: "72157692657241274",
                title: "Despacho Primera Dama exhorta a la población a fomentar valores espirituales, morales y ciudadanos",
                is_public: 1
            },
            {
                id: 308,
                photoset_order: 6145,
                photoset_id: "72157665747231428",
                title: "José Ramón Peralta afirma va firme compromiso del Gobierno con las zonas francas",
                is_public: 1
            },
            {
                id: 309,
                photoset_order: 6144,
                photoset_id: "72157691703004191",
                title: "Comisión designada por Danilo Medina da seguimiento a Visita Sorpresa en San Pedro de Macorís",
                is_public: 1
            },
            {
                id: 310,
                photoset_order: 6143,
                photoset_id: "72157691699278601",
                title: "En Espaillat, Revolución Educativa se afianza; Danilo entrega tres escuelas y estancia infantil",
                is_public: 1
            },
            {
                id: 311,
                photoset_order: 6142,
                photoset_id: "72157693419221895",
                title: "Reunión Sector Transporte",
                is_public: 1
            },
            {
                id: 312,
                photoset_order: 6141,
                photoset_id: "72157692620855384",
                title: "Danilo Medina asiste a presentación resultados Banca Solidaria; más de 430 mil mipymes beneficiadas",
                is_public: 1
            },
            {
                id: 313,
                photoset_order: 6140,
                photoset_id: "72157692616820614",
                title: "Ministro de la Presidencia Gustavo Montalvo",
                is_public: 1
            },
            {
                id: 314,
                photoset_order: 6139,
                photoset_id: "72157687783892150",
                title: "Despacho Primera Dama realiza jornada sobre salud de la mujer contra cáncer cérvicouterino y de mama",
                is_public: 1
            },
            {
                id: 315,
                photoset_order: 6138,
                photoset_id: "72157691662282211",
                title: "Primera Dama participa en acto benéfico del Arzobispado de Santo Domingo",
                is_public: 1
            },
            {
                id: 316,
                photoset_order: 6137,
                photoset_id: "72157687771231390",
                title: "Expansión Plaza Ramírez #2018SeráMejor",
                is_public: 1
            },
            {
                id: 317,
                photoset_order: 6136,
                photoset_id: "72157687770839380",
                title: "Agrandar Negocito de Esposa. Maestro Aponte #2018SeráMejor",
                is_public: 1
            },
            {
                id: 318,
                photoset_order: 6135,
                photoset_id: "72157691651104591",
                title: "Me quedo con Banca Solidaria",
                is_public: 1
            },
            {
                id: 319,
                photoset_order: 6134,
                photoset_id: "72157663638382807",
                title: "Los mecánicos somos buena paga",
                is_public: 1
            },
            {
                id: 320,
                photoset_order: 6133,
                photoset_id: "72157691630775821",
                title: "Despacho Primera Dama contribuye a disminuir mortalidad en recién nacidos; dona equipo",
                is_public: 1
            },
            {
                id: 321,
                photoset_order: 6132,
                photoset_id: "72157692575532824",
                title: "En Bonao, 1,190 estudiantes se integran a Jornada Escolar Extendida; Presidente entrega dos escuelas",
                is_public: 1
            },
            {
                id: 322,
                photoset_order: 6131,
                photoset_id: "72157665672865528",
                title: "Somos Carnaval. Las Cachúas de Cabral",
                is_public: 1
            },
            {
                id: 323,
                photoset_order: 6130,
                photoset_id: "72157692572709754",
                title: "Rosa Rita Álvarez: en 2018, Reservas del país colocará $850 MM para microcrédito",
                is_public: 1
            },
            {
                id: 324,
                photoset_order: 6129,
                photoset_id: "72157663622729957",
                title: "En el mercado mundial. Piña de Cevicos #2018SeráMejor",
                is_public: 1
            },
            {
                id: 325,
                photoset_order: 6128,
                photoset_id: "72157693303440555",
                title: "Danilo empodera a porcicultoras y cacaocultores de SPM para convertirlos en clase media",
                is_public: 1
            },
            {
                id: 326,
                photoset_order: 6127,
                photoset_id: "72157665630328038",
                title: "Danilo trabaja los domingos, nosotras también",
                is_public: 1
            },
            {
                id: 327,
                photoset_order: 6126,
                photoset_id: "72157665595507458",
                title: "En Carrizal de Azua. Una alianza bendecida",
                is_public: 1
            },
            {
                id: 328,
                photoset_order: 6125,
                photoset_id: "72157691521564241",
                title: "Gobierno entrega títulos provisionales de solares a familias de El Carrizal, Azua",
                is_public: 1
            },
            {
                id: 329,
                photoset_order: 6124,
                photoset_id: "72157690300237742",
                title: "Alejandro Montas: Planta Depuradora Mirador Norte-La Zurza está avanzada en un 95%",
                is_public: 1
            },
            {
                id: 330,
                photoset_order: 6123,
                photoset_id: "72157693235728655",
                title: "Sánchez optimiza servicios de salud; Danilo Medina entrega hospital totalmente remozado y moderno",
                is_public: 1
            },
            {
                id: 331,
                photoset_order: 6122,
                photoset_id: "72157669384545929",
                title: "Renovación lazos Cuba y RD: ministro Miguel Mejía recibe a secretaria de Federación Mujeres Cubanas",
                is_public: 1
            },
            {
                id: 332,
                photoset_order: 6121,
                photoset_id: "72157663519298777",
                title: "Danilo Medina escucha necesidades de los petromacorisanos",
                is_public: 1
            },
            {
                id: 333,
                photoset_order: 6120,
                photoset_id: "72157665561030818",
                title: "Servidores públicos becados por INAP se formarán en Administración Pública y Estadísticas en la UASD",
                is_public: 1
            },
            {
                id: 334,
                photoset_order: 6119,
                photoset_id: "72157669362230429",
                title: "Danilo Medina da primer palazo para construcción Torre Quirúrgica de clínica Unión Médica del Norte",
                is_public: 1
            },
            {
                id: 335,
                photoset_order: 6118,
                photoset_id: "72157693202951535",
                title: "En Santiago se eleva calidad educativa; Danilo Medina entrega dos escuelas y una estancia infantil",
                is_public: 1
            },
            {
                id: 336,
                photoset_order: 6117,
                photoset_id: "72157692428866594",
                title: "Optic reconoce a la Contraloría; pondera su rápido avance en uso de TIC y Gobierno Electrónico",
                is_public: 1
            },
            {
                id: 337,
                photoset_order: 6116,
                photoset_id: "72157691484755681",
                title: "Personas pobres de Azua son favorecidas con alianza entre el Gobierno y Orphan’s Heart",
                is_public: 1
            },
            {
                id: 338,
                photoset_order: 6115,
                photoset_id: "72157663484319047",
                title: "Reconocimiento de parte de la Optic al MINPRE",
                is_public: 1
            },
            {
                id: 339,
                photoset_order: 6114,
                photoset_id: "72157691456759421",
                title: "Miguel Mejía: Dominicanos debemos sentirnos orgullosos por desempeño de Danilo en Diálogo Venezuela",
                is_public: 1
            },
            {
                id: 340,
                photoset_order: 6113,
                photoset_id: "72157692393540254",
                title: "Danilo Medina entrega elevado La Caleta; ciudadanos ahorrarán tiempo y dinero",
                is_public: 1
            },
            {
                id: 341,
                photoset_order: 6112,
                photoset_id: "72157663462209317",
                title: "Más de 5 mil familias y 10 mil estudiantes reciben terapias psicosociales con Escuelas de Familias",
                is_public: 1
            },
            {
                id: 342,
                photoset_order: 6111,
                photoset_id: "72157663461924407",
                title: "Rápida respuesta: Gobierno entrega un camión y un tractor a productores de piña de Don Juan",
                is_public: 1
            },
            {
                id: 343,
                photoset_order: 6110,
                photoset_id: "72157692380234474",
                title: "Villa Poppy. Río grande, Constanza",
                is_public: 1
            },
            {
                id: 344,
                photoset_order: 6109,
                photoset_id: "72157665495939238",
                title: "Más inversiones y empleos para el Sur: Danilo Medina asiste a inauguración Club de Playa Puntarena",
                is_public: 1
            },
            {
                id: 345,
                photoset_order: 6108,
                photoset_id: "72157690222768362",
                title: "República Dominicana cumplió con su deber: Danilo Medina",
                is_public: 1
            },
            {
                id: 346,
                photoset_order: 6107,
                photoset_id: "72157693146737325",
                title: "Gran Industria. Hojuelas del Bois #2018SeráMejor",
                is_public: 1
            },
            {
                id: 347,
                photoset_order: 6106,
                photoset_id: "72157692363744764",
                title: "Diálogo Venezuela: partes reciben documento para observaciones; proceso se reanuda este miércoles",
                is_public: 1
            },
            {
                id: 348,
                photoset_order: 6105,
                photoset_id: "72157691423381721",
                title: "Gobierno inicia trabajos de reactivación productiva en Los Tumbados II de Villa Fundación, Peravia",
                is_public: 1
            },
            {
                id: 349,
                photoset_order: 6104,
                photoset_id: "72157693122217025",
                title: "Gobierno lanza planes de contingencia ante terremotos para Santiago y Puerto Plata",
                is_public: 1
            },
            {
                id: 350,
                photoset_order: 6103,
                photoset_id: "72157693121563395",
                title: "Turismo Artesano. Taller Chacuey, Yamasá. M. Plata.",
                is_public: 1
            },
            {
                id: 351,
                photoset_order: 6102,
                photoset_id: "72157693113718045",
                title: "Mayores ventas. Mercadito Freddy #2018SeráMejor",
                is_public: 1
            },
            {
                id: 352,
                photoset_order: 6101,
                photoset_id: "72157691410396091",
                title: "Triplicar las ventas. Iamn Store #2018SeráMejor",
                is_public: 1
            },
            {
                id: 353,
                photoset_order: 6100,
                photoset_id: "72157692341264474",
                title: "400 mil mofongos. Mofongo Mi Terraza. #2018SeráMejor",
                is_public: 1
            },
            {
                id: 354,
                photoset_order: 6099,
                photoset_id: "72157692339270464",
                title: "Maquinarias y nuevos diseños. CalzadoTu Moda #2018SeráMejor",
                is_public: 1
            },
            {
                id: 355,
                photoset_order: 6098,
                photoset_id: "72157690186353382",
                title: "Franquicia y más calidad. Ke pollo. #2018SeráMejor",
                is_public: 1
            },
            {
                id: 356,
                photoset_order: 6097,
                photoset_id: "72157663394988717",
                title: "Padres del sector San Isidro ahorrarán hasta RD$5,000 con liceo entregado por Danilo Medina",
                is_public: 1
            },
            {
                id: 357,
                photoset_order: 6096,
                photoset_id: "72157690163582532",
                title: "Mes de la Patria: Despacho de la Primera Dama se ilumina con los colores patrios",
                is_public: 1
            },
            {
                id: 358,
                photoset_order: 6095,
                photoset_id: "72157669250217809",
                title: "Registro Único de Productores Agropecuarios (RUPA), para consolidar estadísticas del sector",
                is_public: 1
            },
            {
                id: 359,
                photoset_order: 6094,
                photoset_id: "72157690157022602",
                title: "Servicio al cliente. Lorena Vargas Makeup & Nail Spa. #2018SeráMejor",
                is_public: 1
            },
            {
                id: 360,
                photoset_order: 6093,
                photoset_id: "72157665427043818",
                title: "Pacas al por mayor. Tienda Faustina. # 2018SeráMejor",
                is_public: 1
            },
            {
                id: 361,
                photoset_order: 6092,
                photoset_id: "72157693069780885",
                title: "Triplicar raciones. Catering Josa. #2018SeráMejor",
                is_public: 1
            },
            {
                id: 362,
                photoset_order: 6091,
                photoset_id: "72157663344616697",
                title: "Monte Plata: productores de piña reciben apoyo para sembrar nueva variedad y aumentar ingresos",
                is_public: 1
            },
            {
                id: 363,
                photoset_order: 6090,
                photoset_id: "72157692237054714",
                title: "Un mejor futuro para el país; Gobierno avanza en la implementación de República Digital",
                is_public: 1
            },
            {
                id: 364,
                photoset_order: 6089,
                photoset_id: "72157690084966232",
                title: "Primera Dama asiste a calentamiento atletas de  Olimpiadas Especiales en inauguración Copa Davis",
                is_public: 1
            },
            {
                id: 365,
                photoset_order: 6088,
                photoset_id: "72157692204877034",
                title: "Habitantes Mata Mondongo y La Yuca se beneficiarán con instalación redes sanitarias y agua potable",
                is_public: 1
            },
            {
                id: 366,
                photoset_order: 6087,
                photoset_id: "72157692202783554",
                title: "Estudiantes de San Cristóbal reciben con gran ilusión nuevo edificio del Politécnico Loyola",
                is_public: 1
            },
            {
                id: 367,
                photoset_order: 6086,
                photoset_id: "72157690059374022",
                title: "Danilo Medina recibe visita de cortesía delegación senado y cámara de diputados de Haití",
                is_public: 1
            },
            {
                id: 368,
                photoset_order: 6085,
                photoset_id: "72157692192582304",
                title: "Promesa cumplida: productores de jengibre de Samaná ya tienen sus bombas mochilas",
                is_public: 1
            },
            {
                id: 369,
                photoset_order: 6084,
                photoset_id: "72157690049961732",
                title: "Dominicana Limpia mejorará 8 destinos finales que gestionan el 72% de los residuos sólidos del país",
                is_public: 1
            },
            {
                id: 370,
                photoset_order: 6083,
                photoset_id: "72157665320645018",
                title: "Contrataciones Públicas extiende horario; recuerda todos los servicios son gratis",
                is_public: 1
            },
            {
                id: 371,
                photoset_order: 6082,
                photoset_id: "72157692943322125",
                title: "Inauguración Museo Carnaval de la Vega",
                is_public: 1
            },
            {
                id: 372,
                photoset_order: 6081,
                photoset_id: "72157692941755085",
                title: "Presidente juramenta Comisión Reguladora de Prácticas Desleales en el Comercio",
                is_public: 1
            },
            {
                id: 373,
                photoset_order: 6080,
                photoset_id: "72157663252542657",
                title: "Veganos felices con Hospital Luis Morillo King completamente remozado y equipado",
                is_public: 1
            },
            {
                id: 374,
                photoset_order: 6079,
                photoset_id: "72157663251665827",
                title: "Les gusta mi sazón",
                is_public: 1
            },
            {
                id: 375,
                photoset_order: 6078,
                photoset_id: "72157691235713441",
                title: "República Digital avanza con computadoras en las escuelas, nuevos servicios digitales y puntos wifi",
                is_public: 1
            },
            {
                id: 376,
                photoset_order: 6077,
                photoset_id: "72157691234359771",
                title: "Expresiones culturales presentes todo el año: Danilo Medina entrega Museo del Carnaval Vegano",
                is_public: 1
            },
            {
                id: 377,
                photoset_order: 6076,
                photoset_id: "72157691232321821",
                title: "Hospital Morillo King, La Vega. Me lo pusieron nuevecito",
                is_public: 1
            },
            {
                id: 378,
                photoset_order: 6075,
                photoset_id: "72157692888826755",
                title: "Quinta ronda diálogo Venezuela concluye con un solo tema pendiente; partes consultarán en Caracas",
                is_public: 1
            },
            {
                id: 379,
                photoset_order: 6074,
                photoset_id: "72157663228412757",
                title: "Danilo Medina inaugura nuevo edificio de oficinas del obispado de San Pedro de Macorís",
                is_public: 1
            },
            {
                id: 380,
                photoset_order: 6073,
                photoset_id: "72157692126511744",
                title: "Cacaocultores Villa Altagracia reciben camión; iniciarán centro de fermentación, vivero y secaderos",
                is_public: 1
            },
            {
                id: 381,
                photoset_order: 6072,
                photoset_id: "72157692125568874",
                title: "Democratización desayuno, almuerzo y merienda escolar impacta a Mipymes de todo el país",
                is_public: 1
            },
            {
                id: 382,
                photoset_order: 6071,
                photoset_id: "72157692116909644",
                title: "La primera dama Cándida Montilla de Medina expresa su reconocimiento a la juventud en su Día Nacional",
                is_public: 1
            },
            {
                id: 383,
                photoset_order: 6070,
                photoset_id: "72157692871865605",
                title: "Tecnificar e innovar. Panadería Ocoa. #2018SeráMejor",
                is_public: 1
            },
            {
                id: 384,
                photoset_order: 6069,
                photoset_id: "72157669071212889",
                title: "Más empleos a mujeres. Chocolala #2018SeráMejor",
                is_public: 1
            },
            {
                id: 385,
                photoset_order: 6068,
                photoset_id: "72157691197485461",
                title: "Gobierno impulsa preservación de la Cuenca del Río Yaque del Norte",
                is_public: 1
            },
            {
                id: 386,
                photoset_order: 6067,
                photoset_id: "72157663214353887",
                title: "Otro salón. Mint Nailbar & Salón",
                is_public: 1
            },
            {
                id: 387,
                photoset_order: 6066,
                photoset_id: "72157689983687812",
                title: "Maquinarias nuevas, pisos y bates. PROBAMBÙ #2018SeráMejor",
                is_public: 1
            },
            {
                id: 388,
                photoset_order: 6065,
                photoset_id: "72157663212825687",
                title: "Suplir más escuelas. Panadería La fe. #2018SeráMejor",
                is_public: 1
            },
            {
                id: 389,
                photoset_order: 6064,
                photoset_id: "72157669068580399",
                title: "Venderemos 100% más. COOPBAMBÚ #2018SeráMejor",
                is_public: 1
            },
            {
                id: 390,
                photoset_order: 6063,
                photoset_id: "72157691193129841",
                title: "Mejores cosechas con Dos Bocas. Agricultores de Vallejuelo #2018SeráMejor",
                is_public: 1
            },
            {
                id: 391,
                photoset_order: 6062,
                photoset_id: "72157689977980692",
                title: "Local y área de bordado. Winston Cepeda uniformes #2018SeráMejor",
                is_public: 1
            },
            {
                id: 392,
                photoset_order: 6061,
                photoset_id: "72157692109027424",
                title: "Más ingresos y más pedidos. Industria textil #2018SeráMejor",
                is_public: 1
            },
            {
                id: 393,
                photoset_order: 6060,
                photoset_id: "72157665233693368",
                title: "Diálogo de negociación entre Gobierno y oposición de Venezuela",
                is_public: 1
            },
            {
                id: 394,
                photoset_order: 6059,
                photoset_id: "72157691171965431",
                title: "#CuidemosYaquedelNorte",
                is_public: 1
            },
            {
                id: 395,
                photoset_order: 6058,
                photoset_id: "72157692827382155",
                title: "Despacho de la Primera Dama reafirma compromiso de hacer el bien",
                is_public: 1
            },
            {
                id: 396,
                photoset_order: 6057,
                photoset_id: "72157691160295601",
                title: "En todos los supermercados. Chocal",
                is_public: 1
            },
            {
                id: 397,
                photoset_order: 6056,
                photoset_id: "72157691159782041",
                title: "Llevaremos el café más lejos",
                is_public: 1
            },
            {
                id: 398,
                photoset_order: 6055,
                photoset_id: "72157665218204958",
                title: "Más atractivos en ecoturismo",
                is_public: 1
            },
            {
                id: 399,
                photoset_order: 6054,
                photoset_id: "72157692798258725",
                title: "Gobierno y Oposición de Venezuela acuerdan continuar mañana diálogo mediado por Danilo Medina",
                is_public: 1
            },
            {
                id: 400,
                photoset_order: 6053,
                photoset_id: "72157669003599229",
                title: "Presidencia socializa plan de emergencia y evacuación de rutas con servidores del Palacio Nacional",
                is_public: 1
            },
            {
                id: 401,
                photoset_order: 6052,
                photoset_id: "72157692751260875",
                title: "A ritmo de merengue, República Dominicana recibe a Vladimir Guerrero con alegría y admiración",
                is_public: 1
            },
            {
                id: 402,
                photoset_order: 6051,
                photoset_id: "72157663116708907",
                title: "Santo Domingo recibe con alegría a Vladimir Guerrero",
                is_public: 1
            },
            {
                id: 403,
                photoset_order: 6050,
                photoset_id: "72157689877285902",
                title: "Danilo Medina apoya a cacaocultores de Villa Altagracia para agregar valor a productos",
                is_public: 1
            },
            {
                id: 404,
                photoset_order: 6049,
                photoset_id: "72157689876430532",
                title: "Con alegría, pueblo de Don Gregorio sale a las calles en espera de su ídolo Vladimir Guerrero",
                is_public: 1
            },
            {
                id: 405,
                photoset_order: 6048,
                photoset_id: "72157691995333054",
                title: "Puerto Rico, Bonao. Piro Almonte",
                is_public: 1
            },
            {
                id: 406,
                photoset_order: 6047,
                photoset_id: "72157689838470972",
                title: "Promesa cumplida; siete camiones para proyectos agroforestales",
                is_public: 1
            },
            {
                id: 407,
                photoset_order: 6046,
                photoset_id: "72157663041999167",
                title: "Banca Solidaria. Nancy Polanco, artesanía. Villa Mella.",
                is_public: 1
            },
            {
                id: 408,
                photoset_order: 6045,
                photoset_id: "72157689811827882",
                title: "Están llegando los fondos a Hato Mayor y Cumayasa",
                is_public: 1
            },
            {
                id: 409,
                photoset_order: 6044,
                photoset_id: "72157691926663304",
                title: "Danilo Medina: “El compromiso que hicieron con nosotros es que el 29 hay diálogo. Está consensuado”",
                is_public: 1
            },
            {
                id: 410,
                photoset_order: 6043,
                photoset_id: "72157692605690825",
                title: "Somos de Barrio. El Centro, Moca. Galletas Martín, Alida Ovalles",
                is_public: 1
            },
            {
                id: 411,
                photoset_order: 6042,
                photoset_id: "72157662968436137",
                title: "La Placeta, Azua. Nelson Antonio Gómez",
                is_public: 1
            },
            {
                id: 412,
                photoset_order: 6041,
                photoset_id: "72157668787436899",
                title: "INFOTEP enfocado en cualificación de su capital humano; analiza retos Formación Técnico Profesional",
                is_public: 1
            },
            {
                id: 413,
                photoset_order: 6040,
                photoset_id: "72157668786786219",
                title: "Mamografías gratis: Despacho de la Primera Dama proyecta realizar más de 40 mil en todo el país",
                is_public: 1
            },
            {
                id: 414,
                photoset_order: 6039,
                photoset_id: "72157690888515961",
                title: "El Centro, Yamasá. Cerámica Taína",
                is_public: 1
            },
            {
                id: 415,
                photoset_order: 6038,
                photoset_id: "72157692530167725",
                title: "El Higüerito, Moca. Muñeca sin rostro",
                is_public: 1
            },
            {
                id: 416,
                photoset_order: 6037,
                photoset_id: "72157664952499678",
                title: "Cancino Adentro. Revisa y actúa",
                is_public: 1
            },
            {
                id: 417,
                photoset_order: 6036,
                photoset_id: "72157692524270455",
                title: "Energía y Minas somete propuesta concesión que marca hito al asegurar al Estado más de 40% beneficio",
                is_public: 1
            },
            {
                id: 418,
                photoset_order: 6035,
                photoset_id: "72157691774574964",
                title: "Mejoramiento Social, Azua. Chacá - Leonidas Mora",
                is_public: 1
            },
            {
                id: 419,
                photoset_order: 6034,
                photoset_id: "72157664944887458",
                title: "Grandes avances: instituciones estatales incrementan el uso de las TIC y Gobierno Electrónico",
                is_public: 1
            },
            {
                id: 420,
                photoset_order: 6033,
                photoset_id: "72157691744298564",
                title: "Danilo Medina sale a Suiza para participar en 48 Reunión Anual del Foro Económico Mundial",
                is_public: 1
            },
            {
                id: 421,
                photoset_order: 6032,
                photoset_id: "72157662871225517",
                title: "Pareja presidencial asiste a misa conmemorativa Día de la Altagracia, en Higüey",
                is_public: 1
            },
            {
                id: 422,
                photoset_order: 6031,
                photoset_id: "72157689611143922",
                title: "Puntarena, turismo avanza hacia el Sur",
                is_public: 1
            },
            {
                id: 423,
                photoset_order: 6030,
                photoset_id: "72157690777272281",
                title: "Danilo responde. Sector Los Libertadores recibe con regocijo anhelada escuela",
                is_public: 1
            },
            {
                id: 424,
                photoset_order: 6029,
                photoset_id: "72157662798307737",
                title: "República Dominicana dedica su participación en FITUR a Pablo Piñero",
                is_public: 1
            },
            {
                id: 425,
                photoset_order: 6028,
                photoset_id: "72157662798273687",
                title: "MITUR e Iberia renuevan acuerdo de promoción internacional",
                is_public: 1
            },
            {
                id: 426,
                photoset_order: 6027,
                photoset_id: "72157691655566404",
                title: "República Dominicana es destino seguro para los visitantes",
                is_public: 1
            },
            {
                id: 427,
                photoset_order: 6026,
                photoset_id: "72157662797380137",
                title: "República Dominicana asume la presidencia protempore del Consejo Centroamericano de Turismo (CCT)",
                is_public: 1
            },
            {
                id: 428,
                photoset_order: 6025,
                photoset_id: "72157690748471001",
                title: "Ministerio Administrativo de la Presidencia recibe, por segunda ocasión, Premio Nacional a Calidad",
                is_public: 1
            },
            {
                id: 429,
                photoset_order: 6024,
                photoset_id: "72157691629962464",
                title: "Entrevista de Nuria a Roberto Rodríguez Marchena",
                is_public: 1
            },
            {
                id: 430,
                photoset_order: 6023,
                photoset_id: "72157664818045208",
                title: "Presidente Danilo Medina recibe a exgobernador de Puerto Rico, Alejandro García Padilla",
                is_public: 1
            },
            {
                id: 431,
                photoset_order: 6022,
                photoset_id: "72157691629037774",
                title: "Simón Lizardo Mézquita: “Dinamismo en turismo es gracias al crecimiento y fortaleza de economía”",
                is_public: 1
            },
            {
                id: 432,
                photoset_order: 6021,
                photoset_id: "72157664807079248",
                title: "Hambre cero: seis nuevos comedores beneficiarán población vulnerable del sur",
                is_public: 1
            },
            {
                id: 433,
                photoset_order: 6020,
                photoset_id: "72157692354540945",
                title: "Para fomentar exportaciones, reducirán en 75% requisitos para renovación del registro sanitario",
                is_public: 1
            },
            {
                id: 434,
                photoset_order: 6019,
                photoset_id: "72157664787987568",
                title: "Rueda De Prensa Consejo Nacional De Competitividad",
                is_public: 1
            }
        ]
    }
};
