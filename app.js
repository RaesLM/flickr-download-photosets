// De ser necesario para ejecutar comandos del Sistema en la terminal
//const exec = require('child_process').exec;
const colors = require('colors');
const Flickr = require('flickr-set-get');
const ProgressBar = require('progress');
const fs = require('fs');

let photosets = require('./resources/Photosets').sets();

const apiKey = '97117ca07a0bc5cf19cbcfaecf10db64';
const apiSecret = 'e608abd0df0ce25a';
const userId = '88364613@N04';

let appName = 'DICOM';
let description = `Aplicación para descargar álbumes de flickr. DICOM by. RAES. ${ colors.blue('Skipped photosets =')} ${colors.green(process.argv[2])}.`;
let getInfo = '';
let count_photoset = (process.argv[2] ? process.argv[2] : 0);
let count_photo_skipped = 0;
let count_photoset_skipped = 0;
let count_photo_done = 0;
let count_permanent_photo_done = 0;
let bar ='' ;

let counter = (process.argv[3] ? process.argv[3] : photosets.length);

console.log(`\n ${colors.bold.cyan(description)}`);

function getPhotoset(photoset) {
    let options = {
        concurrency: 10,
        outputDir: 'Photosets/Backup/' + photoset.photoset_order + ' (' + photoset.id + ')' + ' - ' + photoset.title,
        size: 'Original',
        noOverwrite: false,
        auth: false
    };

    let client = new Flickr(apiKey, options);

    client.on('error', error => {
        count_photoset_skipped++;
        count_photoset++;

        console.log(`\n${colors.bold.yellow(photoset.id)}: ${colors.red(error.message) + '.'} ${count_photoset_skipped} photosets has skipped.`);

        if (count_photoset < counter) {
            getPhotoset(photosets[count_photoset]);
        } else {
            console.log(`\n${colors.bold.green('All downloads has completed!!!')}`);
            console.log(`${colors.cyan('Total photosets:')} ${photosets.length}, ${colors.green('Downloaded:')} ${count_photoset - count_photoset_skipped}, ${colors.yellow('Skipped:')} ${count_photoset_skipped} `);
            console.log(`${colors.cyan('Total photos downloaded:')} ${count_permanent_photo_done},  ${colors.yellow('Photos skipped:')} ${count_photo_skipped}`);

            process.exit();
        }
    });

    client.on('setInfo', setInfo => {
        getInfo = setInfo;

        // Inicio del Progress Bar
        bar = new ProgressBar(colors.bold.blue(colors.blue(appName + ':')) + ' :bar :current/:total :percent', {
            total: getInfo.total,
            complete: colors.green('█'),
            incomplete: '░',
            width: 100,
        });

        console.log(`\n${colors.bold.cyan(photoset.id)}: Downloading ${getInfo.total} photos from "${getInfo.title}" by ${getInfo.ownername}`)
    });

    // client.on('photoSizes', photoSizes => console.log(photoSizes));

    // Mostrar información cuando se descarga una foto
    client.on('photoDownloaded', photoDownloaded => {
        count_permanent_photo_done++;
        count_photo_done++;
        //console.log(colors.cyan(photoDownloaded));
        bar.tick();

        if (bar.curr === (getInfo.total / 2)) {
            bar.interrupt(colors.cyan('Created By. RAES'));
        }
    });

    // Imprimir si se omitio una foto
    client.on('photoSkipped', photoSkipped => {
        count_photo_skipped++;
        console.log(colors.red(photoSkipped));
    });

    // Imprimir resumen de la descarga al finalizar
    client.on('done', done => {
        count_photoset++;

        console.log(`${colors.bold.green('Photoset done')}. ${count_photo_done} photos downloaded, ${count_photo_skipped} skipped`);

        if (count_photoset < counter) {
            count_photo_done = 0;
            getPhotoset(photosets[count_photoset]);
        } else {

            console.log(`\n${colors.bold.green('All downloads has completed!!!')}`);
            console.log(`${colors.cyan('Total photosets:')} ${photosets.length}, ${colors.green('Downloaded:')} ${count_photoset - count_photoset_skipped}, ${colors.yellow('Skipped:')} ${count_photoset_skipped} `);
            console.log(`${colors.cyan('Total photos downloaded:')} ${count_permanent_photo_done},  ${colors.yellow('Photos skipped:')} ${count_photo_skipped}`);

            process.exit();
        }
    });

    client.downloadSet(photoset.photoset_id, userId);
}
// Ejecutar la función que descarga los photosets desde Flickr
getPhotoset(photosets[count_photoset]);
